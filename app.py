import pandas as pd
import implicit
from sklearn.model_selection import train_test_split
import scipy.sparse as sps
import numpy as np
from flask import Flask
import os

basePath = os.path.dirname(os.path.abspath(__file__))

artists = pd.read_json(basePath + "/yandex_music/artists.jsonl", orient='records', lines=True)
events = pd.read_csv(basePath + "/yandex_music/events.csv")

train, test = train_test_split(events, test_size=0.05, random_state=0)
item_user_train = sps.csc_matrix((1 + 10 * np.log2(train.plays + 1), (train.artistId, train.userId)))
model = implicit.als.AlternatingLeastSquares(factors=32, iterations=10, random_state=0)
model.fit(item_user_train)

app = Flask(__name__)

@app.route('/')
@app.route('/<path:path>')
def get_artist(path=""):
    if path and len(artists[artists['artistName'] == path]):
        return artists.loc[model.similar_items(artists[artists['artistName'] == path]['artistId'].iloc[0])[1][0]].artistName
    else:
        return events.merge(artists)[['artistName', 'plays']].groupby("artistName").sum().sort_values('plays', ascending=False).head(1).iloc[0].name


if __name__ == '__main__':
    app.run("0.0.0.0")